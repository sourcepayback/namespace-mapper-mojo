package eu.fincon.plugin.jaxb.utils;

import org.testng.annotations.Test;

import java.io.File;
import java.net.URL;

public class MappingCacheTest {

	@SuppressWarnings("squid:S00100")
	@Test
	public void test_MappingCache() {
		File mappingFile = loadFile("mapping.csv");
		new MappingCache(mappingFile);
		// successfully loaded
	}

	private File loadFile(String filename) {
		URL url = getClass().getClassLoader().getResource(filename);

		if (null == url) {
			throw new NullPointerException("Can not load file from resources");
		}

		return new File(url.getFile());
	}

	@SuppressWarnings("squid:S00100")
	@Test(expectedExceptions = ArrayIndexOutOfBoundsException.class)
	public void test_mappingCache_format_error() {
		// failure, one line in mapping_error.csv has two much elements
		File mappingFile = loadFile("mapping_error.csv");
		new MappingCache(mappingFile);
	}

	@SuppressWarnings("squid:S00100")
	@Test
	public void test_mappingCache_duplicateEntry() {
		File mappingFile = loadFile("mapping_duplicate_entry.csv");
		new MappingCache(mappingFile);
		// successfully loaded, duplicate entries will be ignore
	}

	@SuppressWarnings("squid:S00100")
	@Test(expectedExceptions = IllegalArgumentException.class)
	public void test_mappingCache_error_loading_mapping_file() {
		new MappingCache(new File("not_exist.csv"));
	}
}