@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.bipro.net/namespace/basis", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED, xmlns = {
    @javax.xml.bind.annotation.XmlNs(namespaceURI = "http://www.bipro.net/namespace/basis", prefix = "basis")
})
package com.ergo.bipro.domain.api.v26.basis;
